<?php

/**
 * static class - helper funkcije
 */
class Helper
{
    /**
     * shuffle asociativnega arraya
     *
     * @param $array
     * @return bool
     */
    public static function shuffle_assoc(&$array)
    {
        $keys = array_keys($array);

        shuffle($keys);

        foreach ($keys as $key) {
            $new[$key] = $array[$key];
        }

        $array = $new;

        return true;
    }

    /**
     * nariše html5 progress bar
     *
     * @param $max
     */
    public static function showProgressBar($max)
    {
        echo "</pre>Searching (".$max." iterations): <progress id='progressbar' value='0' max='" . ($max - 1) . "'></progress><pre>";

    }

    /**
     * inkrement progress bara
     *
     * @param $value
     */
    public static function incrementProgressBar($value)
    {
        echo "<script>document.getElementById('progressbar').value = '" . $value . "';</script>";
        flush();
        ob_flush();
    }
}