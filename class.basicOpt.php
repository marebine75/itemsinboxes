<?php

/**
 * f. za podporo osnovne rešitve, brez lokalne optimizacije;
 * najhitrejša, najslabši rezultat
 */
class basicOpt extends itemsInBoxes {

    /**
     * nakupuje boxe dokler ni zadostne količine
     *
     * @return array
     *
     */
    private function purchaseBoxesSimple($output = true)
    {
        // 1. gre čez vse items
        foreach ($this->items as $item => $itemQuantity) {
            // 2. gre čez vse boxes
            foreach ($this->filteredBoxes as $box => $popularity) {
                // če je item v boxu IN še ni zadostne količine, potem nabavi box
                if (in_array($item, array_keys($this->boxes[$box])) && !$this->checkItemPurchased($item)) {
                    $boxesToPurchase = $this->getNeededBoxQuantity($item, $this->boxes[$box][$item]);
                    if ($boxesToPurchase > 0) $this->purchaseBox($box, $boxesToPurchase, $output, 0);
                }
            }
        }
    }

    /**
     * osnovna rešitev, brez lokalne optimizacije
     */
    public function basicSolution()
    {
        $timerStart = microtime(true);

        $evaluation = array();
        $purchaseItems = array();

        // 1. filtriramo boxe, da dobimo samo tiste, ki vsebujejo želene izdelke
        $this->filteredBoxes = $this->boxFilter();
        //var_dump($this->filteredBoxes);

        echo "<pre>list of desired items:<br>";
        print_r($this->getItems());

        // 2. nakupovanje boxov
        echo "<pre><br><br>purchased boxes (with items): <br>";
        $this->purchaseBoxesSimple();

        echo "<br><br>purchased items status (minus means overhead items, 0 is optimal): <br>";
        var_dump($this->getPurchasedItems());

        echo "<br>all items purchased: " . ($this->checkAllPurchased() ? 'yes' : 'no');


        echo "<br>sum(abs(items quantity)) = " . $this->evaluateSolution($this->purchasedItems) . " (smaller is better)";

        $timerEnd = microtime(true);
        echo "<br>Time elapsed: " . ($timerEnd - $timerStart);
    }
}