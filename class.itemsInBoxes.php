<?php

/**
 * Class itemsInBoxes
 *
 * framework za iskanje rešitve problema nakupovanja želenih izdelkov preko paketov izdelkov
 */
class itemsInBoxes
{

    protected $items;
    protected $boxes;

    /**
     * @var na začetku je enaka $this->items, potem pa odšteva ob nakupih
     */
    protected $purchasedItems;
    protected $filteredBoxes = array();
    protected $purchasesStore = array();

    //public $timeConsumed;

    public function __construct($items, $boxes)
    {
        $this->items = $items;
        $this->boxes = $boxes;
        $this->initializePurchasedItems();
    }

    protected function initializePurchasedItems()
    {
        $this->purchasedItems = array();
        $this->purchasedItems = $this->items;
    }

    protected function getItems()
    {
        return $this->items;
    }

    protected function getPurchasedItems()
    {
        return $this->purchasedItems;
    }

    protected function getBoxes()
    {
        return $this->boxes;
    }

    /**
     * oceni rešitev: manjša številka pomeni boljši rezultat, 0 pomeni optimalno rešitev
     */
    protected function evaluateSolution($purchasedItems)
    {
        $sum = 0;
        foreach ($purchasedItems as $quantity) {
            $sum += abs($quantity);
        }
        return $sum;
    }

    /**
     * preveri, če je želena količina posameznega izdelka kupljena
     *
     * @param $item     string      id izdelka
     * @return bool
     */
    protected function checkItemPurchased($item)
    {
        return $this->purchasedItems[$item] <= 0;
    }

    /**
     * preveri, če so vsi izdelki kupljeni
     *
     * @return bool
     */
    protected function checkAllPurchased()
    {
        foreach ($this->purchasedItems as $quantity) {
            if ($quantity > 0) return false;
        }
        return true;
    }

    /**
     * shrani podrobnosti nakupa za prikaz na koncu
     *
     * @param $boxesToPurchase
     * @param $box
     * @param $idx
     * @param $type   string     'STORNO' = storniranje nakupa
     */
    protected function storePurchase($boxesToPurchase, $box, $idx, $type = 'BUY')
    {
        //echo "purc: " . $boxesToPurchase . " " . $box;
        if ($type == 'BUY') {
            $this->purchasesStore[$idx][$box] += $boxesToPurchase;
        } else {
            $this->purchasesStore[$idx][$box] -= $boxesToPurchase;
        }
    }

    /**
     * kupi box (odšteje quantity v purchasedItems)
     *
     * @param $box
     * @param $boxesToPurchase
     * @param $output
     * @param $idx
     * @param $type   string     'STORNO' = storniranje nakupa
     */
    protected function purchaseBox($box, $boxesToPurchase, $output, $idx, $type = 'BUY')
    {
        if ($output) {
            echo $boxesToPurchase . " boxes " . $box . " " . ($type == 'STORNO' ? 'cancelled' : 'purchased') . ", items: <br>";
            flush();
            ob_flush();
        }

        $boxItems = $this->boxes[$box];
        foreach ($boxItems as $item => $quantity) {
            if ($type == 'BUY') {
                $this->purchasedItems[$item] -= ((int)$quantity * $boxesToPurchase);
            } else {
                $this->purchasedItems[$item] += ((int)$quantity * $boxesToPurchase);
            }
            if ($output) {
                echo "&nbsp;&nbsp;" . $item . ' = ' . $quantity . '<br>';
                flush();
                ob_flush();
            }
        }

        $this->storePurchase($boxesToPurchase, $box, $idx, $type);

    }

    /**
     * naključnp razveljavi nakupe boxov
     *
     * @param $idx
     * @param $output
     * @param $noOfBoxes    integer     število nakupov za razveljavit
     */
    protected function stornoBoxes($idx, $output, $noOfBoxes)
    {
        for ($ii = 1; $ii < $noOfBoxes; $ii++) {
            $this->purchaseBox(array_rand($this->purchasesStore[$idx]), 1, $output, $idx, 'STORNO');
        }
    }

    /**
     * vrne samo boxe z izdelki, ki nas zanimajo
     * (optimizacija v primeru, da bi bilo veliko število boxov z izdelki, ki jih ni med items)
     *
     * @return array
     */
    protected function boxFilter()
    {
        $filteredBoxes = [];

        // 1. gre čez vse items
        foreach ($this->items as $itemKey => $itemQuantity) {
            // 2. gre čez vse boxes
            foreach ($this->boxes as $box => $boxItems) {
                // če je item v boxu, si ga zapomne
                if (in_array($itemKey, array_keys($boxItems))) {
                    //$this->searchItemInBoxes($itemKey, $itemQuantity, $box);
                    $filteredBoxes[$box] += 1;
                }
            }
        }

        //arsort($filteredBoxes);
        //asort($filteredBoxes);

        return $filteredBoxes;
    }


    /**
     * dobi potrebno število boxov za kupit, da zadosti danemu $item
     */
    protected function getNeededBoxQuantity($item, $noOfItemsInBox)
    {
        //echo 'getNeededBoxQuantity' . $item . ' ' . $noOfItemsInBox;
        if ($this->purchasedItems[$item] <= 0) return 0;
        else {
            return ceil($this->purchasedItems[$item] / $noOfItemsInBox);
        }
    }


    /**
     * ocena rešitve v primeru nakupa boxa
     *
     * @param $box  string      box, ki ga nameravamo kupiti
     * @param $boxesToPurchase  integer     število boxev za nakup
     *
     * @return integer
     *
     * @see evaluateSolution
     */
    protected function evaluatePurchase($box, $boxesToPurchase)
    {
        $boxItems = $this->boxes[$box];
        $purchasedItems = $this->purchasedItems;

        foreach ($boxItems as $item => $quantity) {
            $purchasedItems[$item] -= ((int)$quantity * $boxesToPurchase);
        }

        return $this->evaluateSolution($purchasedItems);
    }

    /**
     *  preveri, če so v boxu še kakšni artikli, ki jih potrebujemo
     *
     * @return boolean
     */
    protected function itemsInBoxStillMissing($box)
    {
        $boxItems = $this->boxes[$box];
        foreach ($boxItems as $item => $quantity) {
            if ($this->purchasedItems[$item] > 0) return true;
        }
        return false;
    }

    /**
     * vrne max količino potrebnih nakupov za dani $box
     *
     * @param $box
     * @return  integer
     *
     * @deprecated
     */
    protected function getMaxQuantity($box)
    {
        $max = 1;
        $boxItems = $this->boxes[$box];
        foreach ($boxItems as $item => $quantity) {
            if ($this->purchasedItems[$item] > $max) {
                $max = $this->purchasedItems[$item];
            }
        }
        return $max;
    }

    /**
     * poišče box in število, ki najbolj ustreza danemu $item in $quant
     *
     * @param $item     string      item index
     * @param $quant    integer     item quantity
     *
     * @return array    $boxArr[ boxId, boxesToPurchase ]
     */
    protected function getBoxWithItem($item, $quant)
    {
        foreach ($this->filteredBoxes as $box => $pop) {
            if (in_array($item, array_keys($this->boxes[$box]))) {
                return array($box, ceil($quant / $this->boxes[$box][$item]));
            }
        }
    }


    /**
     * krajši pogoj za preverjanje, če se nakup izplača
     *
     * @param $box
     * @param $boxesToBuy
     * @param $purchasedItems
     *
     * @return bool
     */
    protected function isBetterPurchase($box, $boxesToBuy, $purchasedItems)
    {
        return $this->evaluatePurchase($box, $boxesToBuy) < $this->evaluateSolution($purchasedItems);
    }


}