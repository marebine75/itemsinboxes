<?php

/**
 * f. za podporo rešitev z uporabo lokalne optimizacije;
 * počasnejše, boljši rezultat
 */
class localOpt extends itemsInBoxes
{

    /**
     * A: nakupuje boxe tako da gre čez vse boxe
     *
     * @param   $output   boolean  show output
     * @param   $idx     integer   loop index
     *
     * @return array
     */
    private function purchaseByBoxes($output = true, $idx = 0)
    {
        // izstopni pogoj je, ko so vsi izdelki nabavljeni
        while (!$this->checkAllPurchased()) {
            // A: zanka hodi čez vse boxe
            foreach ($this->filteredBoxes as $box => $pop) {
                // (če se nakup izplača AND še niso vsi nabavljeni) OR količina za item še ni dosežena => potem ga kupi
                $boxesToBuy = 1;    // rand(1,2);
                if (($this->isBetterPurchase($box, $boxesToBuy, $this->purchasedItems) AND !$this->checkAllPurchased())
                    OR $this->itemsInBoxStillMissing($box)
                ) {
                    //$this->purchaseBox($box, $this->getMaxQuantity($box), $output);
                    $this->purchaseBox($box, $boxesToBuy, $output, $idx);       // rand(1,2)
                }
            }
        }
    }

    /**
     * B: nakupuje boxe tako da gre čez vse iteme
     *
     * @param   $output   boolean  show output
     * @param   $idx     integer   loop index
     *
     * @return array
     */
    private function purchaseByItems($output = true, $idx = 0)
    {
        // zanka čez vse items
        foreach ($this->items as $item => $quant) {
            $boxArr = $this->getBoxWithItem($item, $quant);
            list($box, $boxesToBuy) = $boxArr;

            if (!$this->checkAllPurchased()) {
                //$boxesToBuy = $this->getNeededBoxQuantity($item, $this->boxes[$box][$item]);
                $this->purchaseBox($box, $boxesToBuy, $output, $idx);
            }
        }
    }

    /**
     * C: nakupuje random
     *
     * @param   $output   boolean  show output
     * @param   $idx     integer   loop index
     *
     * @return array
     */
    private function purchaseByRandom($output = true, $idx = 0)
    {
        $noOfBoxes = sizeof($this->filteredBoxes);
        $minQuantity = 1;   // min($this->items);
        $maxQuantity = 1;   // max($this->items);

        // izstopni pogoj je, ko so vsi izdelki kupljeni
        while (!$this->checkAllPurchased()) {
            $box = array_keys($this->filteredBoxes)[rand(0, $noOfBoxes - 1)];
            $boxesToBuy = 1; // rand($minQuantity, $maxQuantity);

            if ($this->isBetterPurchase($box, $boxesToBuy, $this->purchasedItems) OR $this->itemsInBoxStillMissing($box)) {
                $this->purchaseBox($box, $boxesToBuy, $output, $idx);
            } else {
//                echo "<br>" . $this->evaluateSolution($this->purchasedItems);
//                flush();
//                ob_flush();
            }


        }
    }

    /**
     * @TODO
     *
     * dodatna optimizacija rešitve:
     * STORNO 1..n že kupljenih boxov in spet purchase
     *
     * @param $forEnd
     * @param array $evaluation
     */
    private function stornoAndPurchaseAgain($forEnd, $evaluation = [], $bestIdx)
    {

        for ($jj = 0; $jj < $forEnd; $jj++) {
            $boxesToStorno = rand(1, sizeof($this->purchasesStore[$jj]));
            echo "<br>storno iteration " . $jj . ", boxes storno " . $boxesToStorno;
            $this->stornoBoxes($bestIdx, false, $boxesToStorno);
            $this->purchaseByRandom(false, $bestIdx);
            echo ", evaluation: " . $evaluation[$jj];
            Helper::incrementProgressBar($jj + $forEnd);
        }
    }

    /**
     * iskanje z lokalno optimizacijo
     *
     * @param       BOX - iskanje v zanki čez boxes,
     *              ITEM - iskanje v zanki čez items
     *              RAND - naključno iskanje
     * @param       $forEnd     integer     število ponoviev zanke
     */
    public function localOptSolution($param = BOX, $forEnd = 50)
    {
        $timerStart = microtime(true);

        $evaluation = array();
        $purchaseItems = array();

        // 1. filtriramo boxe, da dobimo samo tiste, ki vsebujejo želene izdelke
        $this->filteredBoxes = $this->boxFilter();
        //var_dump($this->filteredBoxes);

        echo "<pre>list of desired items:<br>";
        print_r($this->getItems());

        // progress bar
        Helper::showProgressBar($forEnd);       // * 2

        // notranja zanka: iskanje najboljše rešitve
        for ($ii = 0; $ii < $forEnd; $ii++) {
            // premeša array in inicializacija
            Helper::shuffle_assoc($this->filteredBoxes);
            Helper::shuffle_assoc($this->items);
            $this->initializePurchasedItems();

            // izbira načina iskanja BOX / ITEM / RAND
            if ($param == BOX) {
                $this->purchaseByBoxes(false, $ii);
            } else if ($param == ITEM) {
                $this->purchaseByItems(false, $ii);
            } else {
                $this->purchaseByRandom(false, $ii);
            }

            $evaluation[$ii] = $this->evaluateSolution($this->purchasedItems);
            $purchaseItems[$ii] = $this->purchasedItems;

            Helper::incrementProgressBar($ii);

        }

        $best = array_keys($evaluation, min($evaluation))[0];       // indeks najboljše izmed variant

        // TODO: dodatna optimizacija najboljše rešitve
        // print_r($purchaseItems[$best]);
        // echo "<br>best: " . $evaluation[$best];
        // $this->stornoAndPurchaseAgain($forEnd, $evaluation, $best);

        echo "<pre><br><br>purchased boxes (with items): <br>";
        foreach ($this->purchasesStore[$best] as $box => $quant) {
            echo "<br>" . $quant . " x " . $box . " with items:<br>";
            print_r($this->boxes[$box]);
        }

        echo "<br><br>purchased items status (minus means overhead items, 0 is optimal): <br>";
        print_r($purchaseItems[$best]);

        echo "<br>all items purchased: " . ($this->checkAllPurchased() ? 'yes' : 'no');

        echo "<br>sum(abs(items quantity)) = " . $evaluation[$best] . " (smaller is better)";

        $timerEnd = microtime(true);
        echo "<br><br>Time elapsed: " . ($timerEnd - $timerStart);
    }

}