/**
 * Created by marko on 15. 05. 2017.
 */
var gulp = require('gulp');
var livereload = require('gulp-livereload');

gulp.task('watch', function () {
    livereload.listen();
    gulp.watch(
        ['**/*.php', '**/*.js', '**/*.css'],
        { interval: 500 },
        livereload.changed
    );
});
