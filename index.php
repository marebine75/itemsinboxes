<?php

set_time_limit(0);
error_reporting(E_ALL & ~E_NOTICE);

/*
items array is [$itemId => $neededQuantity]
This array has the list of items and quantities which I need to buy.
 */
$items = require_once 'items.php';

/*
boxes array is $boxId => [$itemId => $quantityInBox]
This array has the catalog of boxes available to buy.
 */
$boxes = require_once 'boxes.php';

/*
We have boxes which have N items in it with N quantity. Items can be in 1 or more boxes.
How many boxes from which box do I need to buy, to satisfy the needed item quantities?
The provided data should give back: needed box quantity and which items are in the box.
What would be the best combination to have least amount of boxes.
An OOP approach would add more point to the test.
 */

require_once 'class.Helper.php';
require_once 'class.itemsInBoxes.php';
require_once 'class.basicOpt.php';
require_once 'class.localOpt.php';

/////////
// main
/////////

$bas = new basicOpt($items, $boxes);
$loc = new localOpt($items, $boxes);

//$bas->basicSolution();                                 // worst / fastest
//$loc->localOptSolution(ITEM, 10);
//$loc->localOptSolution(BOX, 10);
$loc->localOptSolution(RAND, 50); // best / slowest
